#!/bin/bash

# Needs jupyter nbconvert

script_name="$(basename "${BASH_SOURCE[0]}")"
mod_path="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
nb_path="${mod_path}/MOD.ipynb"

if [[ ${#} < 2 ]]; then
  echo "Usage: ${script_name} SIGFILE OUTFILE"
  exit 1
fi

MODSIGFILE="$(realpath "${1}")"
outfile="$(realpath "${2}")"
export MODSIGFILE

jupyter nbconvert --stdout --execute --to html "${nb_path}" | sed "s/'scripts\/JSRoot\.core', //g" >"${outfile}"
